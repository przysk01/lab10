package pk.labs.LabA;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = null;
    public static String controlPanelImplClassName = null;

    public static String mainComponentSpecClassName = null;
    public static String mainComponentImplClassName = null;
    // endregion

    // region P2
    public static String mainComponentBeanName = null;
    public static String mainFrameBeanName = null;
    // endregion

    // region P3
    public static String sillyFrameBeanName = null;
    // endregion
}
